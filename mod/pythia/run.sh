#!/bin/sh
#SBATCH -t 09:00:00
# specify project
#BATCH -A hep2016-1-5
# specify storage
#SBATCH -p hep

# This is the skript that runs the generator. 
# As a result the file new.yoda should be present. 

sleep 3
cat ../../../infiles/run.cmnd >> params.cmnd
cat had.in >> params.cmnd
main93 -c params.cmnd -s 1 -n 100000
sleep 1
mv Rivet.yoda new.yoda
sleep 3
