#!/bin/bash

# This file should return the number of jobs currently running on the cluster.
# It is used to wait untill new jobs can be put on the cluster as well as to wait until
# next steps can be performed. (Currently we assume the tuneing jobs are the only jobs run on the cluster.)

echo $(squeue -u jbellm |wc -l)-1|bc
