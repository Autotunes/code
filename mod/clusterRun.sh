#!/bin/bash

# This is used to put the bash skripts on the cluster. 
#  $1 is the skript.
#  $2 ... $4 could be arguments. (If your cluster setup does not allow arguments you will have to modify a bit more.)

sbatch $1 $2 $3 $4

