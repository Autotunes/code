#!/bin/sh
#SBATCH -t 09:00:00
# specify project
#BATCH -A hep2016-1-5
# specify storage
#SBATCH -p hep


# This is the skript that runs the generator. 
# As a result the file new.yoda should be present. 

source /home/jbellm/opt/bin/activate
cp $1/infiles/new.in .
cp $1/infiles/ana.in .
sleep 1
Herwig read new.in
sleep 1
Herwig run new.run  -N100000
rm -f new.run  new.tex new.out new-EvtGen.log ana.in param_card.dat new.in
rm -rf Herwig-cache
