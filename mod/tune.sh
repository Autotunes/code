#!/bin/sh
#SBATCH -t 09:00:00
# specify project
#BATCH -A hep2016-1-5
# specify storage
#SBATCH -p hep


# Here we perform the tuning with professor. 
# Obviously professor but also rivet needs to be installed and callable. 
# Make sure it is present in the path variables needed (also on the cluster). 

source /home/jbellm/opt/bin/activate
prof2-ipol scan --wfile=weights.dat --order=3 --rc=runcombs.dat:$1 ipol_folder/ipol$1.dat
prof2-tune -d $(rivet-config --datadir) --filter ipol_folder/ipol$1.dat --wfile=weights.dat --outdir=tune_folder/tune$1
