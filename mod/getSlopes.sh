#!/bin/sh
#SBATCH -t 09:00:00
# specify project
#BATCH -A hep2016-1-5
# specify storage
#SBATCH -p hep

# This file is also to perform the bash skript on the cluster. 
python $AUTOTUNEPATH/lib/getSlopes.py
