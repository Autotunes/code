#!/bin/sh
# Run Tune step

source $AUTOTUNEPATH/env.sh
# Get had.in from ../infiles/ and replace parameters form startvalues
# or the results of previous tune steps
python $AUTOTUNEPATH/lib/gethad.py
# Produce the scan folder
prof2-sample -n300 -t had.in param.dat
# Run your generator. An example setup is given in run.sh
for i in $(ls scan);
do
    while [ "$( bash $checkruns )" -ge "300"  ];
    do  
        bash $checkruns
        sleep 10;
    done ;
    (cd scan/$i;bash $AUTOTUNEPATH/mod/clusterRun.sh $AUTOTUNEPATH/mod/run.sh $AUTOTUNEPATH);
done

# Create run combinations
#   - first number is the number of runs removed in this combination
#.  - second is number for combinations.
prof2-runcombs scan 100:100

# Create folders
mkdir -p ipol_folder
mkdir -p tune_folder

# Wait for unfinished jobs
while [ "$( bash $checkruns )" -gt "0"  ];
do    
  sleep 10;
done ;

# Now interpolate and then tune
for i in $(seq 0 99);
do
    bash $AUTOTUNEPATH/mod/clusterRun.sh $AUTOTUNEPATH/mod/tune.sh $i;
done

# Wait for unfinished jobs.
while [ "$( bash $checkruns )" -gt "0"  ];
do    
  sleep 10;
done ;

# Paste all results into res.dat
cat  tune_folder/tune*/results.txt > res.dat

# Extract best tune and make validation plots
python $AUTOTUNEPATH/lib/analyse.py



