# In this file we take the template had.in file and insert the values
# given in the tune.res.dat file first and then from the param.other.dat file.
# param.other.dat contains the parameters given before the sub-tunes have been
# performed.
# Once the values of the parameters have been replaced for the parameters that
# should not be tuned in this subtune, professor can take the written had.in (local)
# and make the replacements in prof-sample.

# This script is called to prepare the subtune in the corresponding subtune-folder.

import os
rep=[]

# Check for already improved values from other subtunes.
try:
  other=open("%s/tune.res.dat"%(os.environ["AUTOTUNEPATH"]),"r")
  for line in other:
    rep+=[line.replace("\n","").split(" ")]
except:
  print "No tune.res.dat yet..."

# Now get the parameter values that have not yet been tuned
# from the last iteration.
# param.other.dat is written in splitSteps.py
other=open("param.other.dat","r")
for line in other:
  rep+=[line.replace("\n","").split(" ")]

had=open("%s/infiles/had.in"%(os.environ["AUTOTUNEPATH"]),"r")
hadn=open("had.in","w")
for line in had:
  for r in rep:
    line=line.replace("{%s}"%r[0],r[1])
  hadn.write(line)
