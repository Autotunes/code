# Go through slopes and find 'correlated' parameters, i.e., parameters
# that influence the same data bins.

# Import helpers
import sys,os
sys.path.insert(0,"%s/lib/"%(os.environ["AUTOTUNEPATH"]))
from AutoTuneHelpers import *
if os.environ["AUTOTUNEPATH"] != os.environ["PWD"]:
   print "PWD is not AUTOTUNEPATH: source env.sh"
   exit()

# Load slopes from Search step
sorig = pickle.load( open( "SearchStep/save.p", "rb" ) )
s = [x for x in sorig if sum(x[0])==sum(x[0])]
TuneSteps=int(os.environ["AUTOTUNE_NSTEPS"])


# Find normalization for s in each parameter to reduce effect of
# the overall influence of a parameter
snorm=sum([x[0] for x in s])

# Produce all possible combinations
Combinations=set(itertools.combinations(range(ndim),int(math.floor( float(ndim)/TuneSteps))))
lenCombinations=len(Combinations)
print "We have to check ",lenCombinations,"combinations..."
J_used=np.zeros(ndim)

# Normalize s such that sum_i s_i = (1,1,1,...,1,1)
stmp=np.array([x[0] for x in s])
sn=stmp/snorm

# Split the tune up into steps
for i in range(TuneSteps)[::-1]:
    # Comment to [::-1]: We reverse the order of TuneSteps
    #                    as the highest weight while splitting the 
    #                    runs usually comes from parameters strongly 
    #                    correlated in few observables.
    #                    Idea: First optimise parameters that are influential
    #                          in many observables, then optimise the parameters
    #                          that effect fewer observables. 

    # Create folder
    if not os.path.isdir("TuneStep%s"%i):
        os.mkdir("TuneStep%s"%i)
    
    # Find parameter set for respective step
    print "\nSearching Parameters for Step %s:"%i
    wmax=0.
    maxcomb=0
    maxcombstr=0
    count=0
    if i!=0: # For all but the last (first when tuning) step:
      for aa in Combinations:
        count+=1
        print "\r%s %%"%(int(float(count)/lenCombinations*100)),
        J=np.zeros(ndim)
        for t in range(int(math.floor( float(ndim)/TuneSteps))):
          J[aa[t]]=1
        # -> J=(0,...0,1,0..,1,..) 
        if sum(J_used*J)!=0.:
            continue
        # For all bins project normalised s on J then square,
        # corresponding to eq. 4 in arXiv:1908.10811
        w=sum(np.power(sum((sn*J).transpose()),2))
        if wmax<w:
            wmax=w
            maxcomb=np.array(aa)
            maxcombstr=[plines[aaa].split()[0] for aaa in aa]
            print "Found new wmax:",w,"current combination",maxcombstr

    # Last (first when tuning) step collects remaining parameters
    else: 
        maxcomb=np.where(J_used<1)[0]
        maxcombstr=[plines[aaa].split()[0] for aaa in maxcomb]
            
    # Write parameter files indicating which parameters to tune and
    # which ones to fix by initial guess/input or previous tune steps
    step=open("TuneStep%s/param.dat"%(i),"w")
    other=open("TuneStep%s/param.other.dat"%(i),"w")
    
    for aaa in range(ndim): 
        par=plines[aaa].split()[0]
        if aaa in maxcomb:
            step.write("%s %s %s"%(par,ranges[par][0],ranges[par][1])+"\n")
        else:
            if J_used[aaa]:
                other.write("%s %s"%(par,startvalues[par])+"\n")
    step.close()
    other.close()

    # Write weight file based on eq. 5 in arXiv:1908:10811
    # sum(snorm) is just a constant, identical for all data bins
    J=np.zeros(ndim)
    J[maxcomb]=1
    Jsnorm=J/snorm*sum(snorm)
    Jall=np.ones(ndim)
    Jallsnorm=Jall/snorm*sum(snorm)
    step=open("TuneStep%s/weights.dat"%(i),"w")
    for bi in s:
        step.write("%s#%s:%s %s"%(bi[1],bi[2],bi[2]+1,sum(bi[0]*Jsnorm)*sum(bi[0]*Jsnorm)/sum(bi[0]*Jallsnorm)))
        step.write("\n")
    step.close()
    
    # Bookkeeping of tuned parameters
    J_used[maxcomb]=1
    print "\nParameters for Step %s :\n"%(i),maxcombstr    


