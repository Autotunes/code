# Here we calculate the slope vectors for the method to calculate the
# weights and to split the subtunes.
# At the end of the file we write out the slopes to be used in other scripts.

# This script is called when the search step has called the generator to fill
# the histograms and a potential best guess tune is performed.

# Here we only calculate the raw-slopes "S".
# Further normalisation to "N" from 1908.10811 is performed in split steps.

import sys,os
sys.path.insert(0,"%s/lib/"%(os.environ["AUTOTUNEPATH"]))
from AutoTuneHelpers import *


if os.environ["PWD"]!=os.environ["AUTOTUNEPATH"]:
   print "PWD is not AUTOTUNEPATH" 
   exit()

# # Before you go on, make sure all runs are done and finished.
print "Get the yodas:"
global yodas
yodas=[]
for i in range(0,(int(os.environ["AUTOTUNE_NFIND"]) )):
    if os.path.isfile("SearchStep/scan/%s/new.yoda"%str(i).rjust(4, '0')):
        yod=yoda.readYODA("SearchStep/scan/%s/new.yoda"%str(i).rjust(4, '0'))
          # Currently we only deal with historgrams that can
          # be translated to Scatter2D
        scatteryoda=yod.copy()
        scatteryoda.clear()
        for name in yod.keys():
            sc=yod[name].mkScatter()
            if sc.type=="Scatter2D":
              scatteryoda[name]=sc
        yodas+=[scatteryoda]
    print "\r", i,

print "\nAnd the corresponding parameter points:"
global params
params=[]
for i in range(0,int(os.environ["AUTOTUNE_NFIND"])):
    if os.path.isfile("SearchStep/scan/%s/new.yoda"%str(i).rjust(4, '0')):
        par=open("SearchStep/scan/%s/params.dat"%str(i).rjust(4, '0'),"r")
        par=[line.split() for line in par]
        par=[[line[0],float(line[1])] for line in par]
        params+=[par]
    print "\r", i,
    sys.stdout.flush()
print "\nFound %s yodas"%len(yodas)
if len(yodas) == 0:
  print "Aborting..."
  exit()
# s will contain the slopes
s=[]
y0=yodas[0]
count=0
print "Search slope vectors for %s Histograms"%(len(y0.keys()))

# Loop through all the historgrams that are given in the first yoda-file. 
for k in y0.keys():
    # Do not use the raw histograms from generator data. There is no data. 
    if "RAW" in k: continue
    h0=y0[k]
    xx=k.split("/")
    count+=1
    # Write the current status
    print '\r' , int(float(count)/len(y0.keys())*100) ,"%",
    sys.stdout.flush()
    for i in range(0,h0.numPoints):
        # This step could be parallelized:
        stmp=getSlope("%s"%xx[1],"%s"%xx[2],i,yodas,params)
        s+=[[stmp,k,i]]
print "Found %s slope vectors"%len(s)


# This is to save the state of the slopes
import pickle
print "Found %s slope vectors"%len(s)
pickle.dump( s, open( "SearchStep/save.p", "wb" ) )
