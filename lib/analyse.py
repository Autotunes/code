# Here we analyse the tunes performed with the random sub-set of points given
# by the professor run-combinations method.
# If the parameter makeValidationPlots is true (default) the script
# automatically creates validation plots to illustrate the best fit values in
# scatter plots (see fig 2 in 1908.10811).

# The script is called from one of the subtune folders.


import numpy as np

makeValidationPlots=True

# Try to import matplotlib
try:
  import matplotlib
  matplotlib.use('Agg')
  from matplotlib import pyplot as plt
except ImportError:
  makeValidationPlots = False

# Get the parameters used in this sub-tune:
params=open("param.dat","r")
# In res.dat the results of all run combination tunes are pasted in the same file.
result=open("res.dat","r")
# Open file to write the result of this sub-tune.
tuneres=open("tune.step.res.dat","w")
plines=[li.replace("\n","") if not li.startswith("#") else "" for li in params]
res=[]
cur={}
# Professor writes out the GOF value in the third column.
for line in result:
    if "GOF" in line:
        if len(cur)>0:
            res+=[cur]
            cur={}
        cur["GOF"]=float(line.split()[2])
    if not line.startswith("#") and not line.startswith("-"):
        sp=line.split()
        cur[sp[0]]=float(sp[1])

# Once we filtered the res.dat for the GOF values, we filter for the best 80%
# and create the margin of 20% as described in 1908.10811.
for p in plines:
    sp=p.split()
    if len(sp)<2: continue
    cut=int(0.8*len(res))
    X=np.array([x[sp[0]] for x in sorted(res, key=lambda k: k["GOF"])[0:cut] ])
    Y=np.array([x["GOF"] for x in sorted(res, key=lambda k: k["GOF"])[0:cut] ])
    X2=np.array([x[sp[0]] for x in sorted(res, key=lambda k: k["GOF"])[cut:-1] ])
    Y2=np.array([x["GOF"] for x in sorted(res, key=lambda k: k["GOF"])[cut:-1] ])
    print sp[0],X[np.where(Y==min(Y))][0],
    diff=max(X)-min(X)
    # TODO: This only allows positive parameters.
    print "in",max(0.,min(X)-diff*0.2),max(X)+diff*0.2
    
    try:
      if makeValidationPlots:
        # Plot the best 80%
        plt.plot(X,Y,"o")
        # Plot the worst 20%
        plt.plot(X2,Y2,"o")
        # Plot the best fit value.
        plt.plot([X[np.where(Y==min(Y))][0],
                 X[np.where(Y==min(Y))][0]],[
                 min(Y),
                 max(Y)])

        oldxlim=[float(sp[1]),float(sp[2])]
        newxlim=[max(0.,min(X)-diff*0.2),max(X)+diff*0.2]
        plotxmin=min(oldxlim[0],newxlim[0])-0.1*(max(oldxlim[1],newxlim[1])-min(oldxlim[0],newxlim[0]))
        plotxmax=max(oldxlim[1],newxlim[1])+0.1*(max(oldxlim[1],newxlim[1])-min(oldxlim[0],newxlim[0]))
        # Plot the old limits.
        plt.plot([float(sp[1]),float(sp[1])],[min(Y),max(Y)],color="red",linestyle="--")
        plt.plot([float(sp[2]),float(sp[2])],[min(Y),max(Y)],color="red",linestyle="--")
        # Plot the new limits.
        plt.plot([max(0.,min(X)-diff*0.2),max(0.,min(X)-diff*0.2)],[min(Y),max(Y)],color="green",linestyle="--")
        plt.plot([max(X)+diff*0.2,max(X)+diff*0.2],[min(Y),max(Y)],color="green",linestyle="--")
        plt.title(sp[0])
        plt.savefig("%s.pdf"%sp[0])
        plt.close()
    except Exception as e:
        print "Something went wrong with plotting... no worries!!"
        print e
        
    # TODO: This only allows positive parameters.
    tuneres.write("%s %s in %s %s \n"%(sp[0],X[np.where(Y==min(Y))][0],max(0.,min(X)-diff*0.2),max(X)+diff*0.2))
