# Prepare the Search Step, used as baseline for identification of parameter correlations

# Import and load helpers
import sys,os
sys.path.insert(0,"%s/lib/"%(os.environ["AUTOTUNEPATH"]))
from AutoTuneHelpers import *

# Catch wrong environment
if os.environ["AUTOTUNEPATH"] != os.environ["PWD"]:
   print "PWD is not AUTOTUNEPATH: source env.sh"
   exit()

# Create folder for search run
if not os.path.isdir("SearchStep"):
    os.mkdir("SearchStep")

# Prepare file used with Professor sample
file_for_prof2sample=open("SearchStep/param.dat","w")
ndim=len(plines)
print "Found %s parameters:"%ndim 
for p in plines:
    p=p.split()
    if len(p)!=5:
        print p
        print "Something is wrong in the input"
    ranges[p[0]]=[float(p[3]),float(p[4])]
    startvalues[p[0]]=float(p[1])
    print '{:25}'.format(p[0]),"startvalue:",'{:15}'.format(p[1])," in [",'{:15}'.format(p[3]),",",'{:15}'.format(p[4]),"]"
    file_for_prof2sample.write("%s %s %s\n"%(p[0],p[3],p[4]))
file_for_prof2sample.close()
