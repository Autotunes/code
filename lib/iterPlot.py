# Helper script to create parameter plots over iterations of the tuning procedure.
# Also creates convenient LaTeX table output of final parameters.

# Import packages and set up parameters for plotting
import matplotlib as mpl
import subprocess,os
mpl.use('Agg')
mpl.rcParams['text.usetex'] = True
mpl.rcParams['text.latex.unicode'] = True
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
from matplotlib import pyplot as plt
import numpy as np

# Set up colors for plot
cm = plt.get_cmap('gist_rainbow')
NUM_COLORS = 20
colors = [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)]

# Access files of initial parameters and tune results
iterfolder = "iterations"
iters  = os.popen("find "+iterfolder+" -mindepth 1 -maxdepth 1 -type d").read().split()
iters.sort()
print(iters)
for i in range(len(iters)):
  iters[i] = iters[i] + os.sep
params_start = open(iters[0]+"param.start.dat")
tunes = []
for i in iters:
    tunes.append(open(i+"tune.res.dat"))
    
# Create dictionary of the format: {"key", n/a, [value], [lo], [hi]}
datadict = {}
for line in params_start:
    sp = line.split()
    datadict[sp[0]] = [0, [],[],[]]
    datadict[sp[0]][1] = [float(sp[1])] # Value
    datadict[sp[0]][2] = [float(sp[3])] # Lower bound
    datadict[sp[0]][3] = [float(sp[4])] # Higher bound
for tune in tunes:
    for line in tune:
        sp = line.split()
        datadict[sp[0]][1].append(float(sp[1]))
        datadict[sp[0]][2].append(float(sp[3]))
        datadict[sp[0]][3].append(float(sp[4]))

# Normalize datadict
normdict = {}
for key,i in zip(sorted(datadict),list(range(20))):
    offset = len(datadict)-i
    value = offset+(np.array(datadict[key][1])-datadict[key][2][0])/(datadict[key][3][0]-datadict[key][2][0])
    lo = offset+(np.array(datadict[key][2])-datadict[key][2][0])/(datadict[key][3][0]-datadict[key][2][0])
    hi = offset+(np.array(datadict[key][3])-datadict[key][2][0])/(datadict[key][3][0]-datadict[key][2][0])
    normdict[key] = [truth,value,lo,hi]
    
# Create plot
x = list(range(len(tunes)+1))
fig=plt.figure(figsize=(10, 7))
for key,color in zip(sorted(normdict),colors):
    plt.plot(x,normdict[key][1],label=key,color=color)
    plt.fill_between(x,normdict[key][2],normdict[key][3],color=color,interpolate=True,alpha=0.5)
    plt.xlabel("Iterations")
    plt.ylabel("Tuned Parameters")
    plt.yticks([])
plt.xlim([0,len(iters)+2])
fig.legend(frameon=False,loc="center right" ,ncol=1,fontsize=15)
fig.tight_layout()
plt.savefig("iterations.pdf")
plt.clf()

# Define minimal tex header & footer for tabular parameter output
tex_header = r"""\documentclass{article}
    \usepackage[margin=0.7in]{geometry}
    \usepackage[parfill]{parskip}
    \usepackage[utf8]{inputenc}
    \usepackage{amsmath,amssymb,amsfonts,amsthm,graphicx}
\begin{document}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{iterations}
\end{figure}
\begin{table}
\renewcommand*{\arraystretch}{1.2}
\centering
\begin{tabular}{|c|c|}
\hline
Parameter & Tune result \\ \hline
"""
tex_footer = r"""\hline
\end{tabular}
\end{table}
\end{document}"""

# Write table with tuned parameters and run combination ranges, rounded appropriately
res = open(iters[len(iters)-1]+"tune.res.dat")
out = open("tune.tex","w")
out.write(tex_header)
for line in res:
  line = line.split()
  firstdiff = 0
  value = float(line[1])
  down = float(line[3])
  up = float(line[4])
  while( (int(value) - int(down)<=2) or (int(up) - int(value) <= 2)):
    value *= 10
    down *= 10
    up *= 10
    firstdiff += 1
  value= round(float(line[1]),firstdiff)
  down_diff = round(float(line[1])-float(line[3])+0.5*10**(-firstdiff),firstdiff)
  up_diff = round(float(line[4])-float(line[1])+0.5*10**(-firstdiff),firstdiff)
  string = "{par} & ${val:."+str(firstdiff)+"f}_{{-{dow:."+str(firstdiff)+"f}}}^{{+{up:."+str(firstdiff)+"f}}}$\\\ \n"
  string = string.format(par=line[0],val=value,dow=down_diff,up=up_diff)
  out.write(string)
out.write(tex_footer)
