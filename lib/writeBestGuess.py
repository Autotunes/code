# Write results of BestGuessTune (right after Search step) into
# new param.start.dat

import os,math

# Load original data for ranges and best tune result for central
ranges=open("../param.start.dat.bak","r")
plines=[li.replace("\n","") if not li.startswith("#") else "" for li in ranges]
plines=list(filter(lambda x: x!='', plines))
best=open("bestGuessTune/results.txt","r")
bestlines=[li.replace("\n","") if not li.startswith("#") else "" for li in best]
bestlines=list(filter(lambda x: x!='', bestlines))
ranges = {}
startvalues = {}
start = open("../param.start.dat","w")

# Set central value
for p in bestlines:
  p = p.split()
  startvalues[p[0]] = float(p[1])

# Set ranges and write
for p in plines:
  p= p.split()
  if len(p)!=5:
    print p
    print "Something is wrong in the input"
  ranges[p[0]]=[float(p[3]),float(p[4])]
  start.write("%s %s in %s %s\n"%(p[0], str(startvalues[p[0]]),str(ranges[p[0]][0]),str(ranges[p[0]][1])))

