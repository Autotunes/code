# This file read imported by various scripts.
# It is used to define a few parameters and
# import packages.

# First we inport some packages:
import yoda,rivet,os,itertools,math,pickle
import numpy as np
from scipy import stats

print "AUTOTUNEPATH: ",os.environ["AUTOTUNEPATH"]

### Get the Starting point and ranges for the parameter values.
# The "startvalues" are read but overwritten once the best guess
# tune is performed.

# Read user input:
# TODO: Requires format very close to the given examples.
#       Could check if format is correct.
ranges=open("%s/param.start.dat"%(os.environ["AUTOTUNEPATH"]),"r")
plines=[li.replace("\n","") if not li.startswith("#") else "" for li in ranges]
plines=list(filter(lambda x: x!= '', plines))
ranges={}
startvalues={}
for p in plines:
    p=p.split()
    if len(p)!=5:
        print p
        print "Something is wrong in the input"
    ranges[p[0]]=[float(p[3]),float(p[4])]
    startvalues[p[0]]=float(p[1])
ndim=len(plines)


def getSlope(name,histo,binnr,yodas,params):
  """This function is used to calculate the slopes to 'normalized' theory """
  # It corresponds to \mathcal{S} in 1908.10811
  global ranges
  res=[]
  thisbin=np.array([y["/%s/%s"%(name,histo)].yVals()[binnr] for y in yodas])
  # Loop though parameters:
  for k,xx in zip(range(0,len(params[0])),params[0]):
    if max(thisbin)-min(thisbin)<=0.:  # If no entry -> no slope..
        res+=[0.]
        continue
    # Normalise bin to range in parameter space:
    low=ranges[xx[0]][0]
    up=ranges[xx[0]][1]
    # Normalise parameter range to [0;1]
    xs=np.array([(p[k][1]-low)/(up-low) for p in params])
    # Normalise bin spread to [0;1] 
    ys=(thisbin-min(thisbin))/(max(thisbin)-min(thisbin))
    # Get the slope:
    # Perform linear regression:
    slope, intercept, r_value, p_value, std_err = stats.linregress(xs, ys)
    res+=[abs(slope)]
  # Return vector in n-dimensional space:
  return np.array(res)



