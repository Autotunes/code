#!/bin/sh
# Run Search step, employing user defined scripts mod/clusterRun.sh and mod/run.sh

source $AUTOTUNEPATH/env.sh
echo "Running SearchStep for folder:"
echo $AUTOTUNEPATH
# Get had.in from ../infiles/ 
cp $AUTOTUNEPATH/infiles/had.in .
# Samples
echo "Creating sample points for searchstep."
prof2-sample -n $AUTOTUNE_NFIND -t had.in param.dat

# Run your generator
echo "Start running generator as defined in:"
echo "$AUTOTUNEPATH/mod/run.sh"
for i in $(ls scan);
do
    while [ "$( bash $checkruns )" -gt "250" ];
    do
        echo $( bash $checkruns ) "jobs running"
        sleep 10;
    done ;
    (cd scan/$i;bash $AUTOTUNEPATH/mod/clusterRun.sh $AUTOTUNEPATH/mod/run.sh $AUTOTUNEPATH);
done

# Wait for unfinished jobs
while [ "$( bash $checkruns )" -gt "0"  ];
do
  echo $( bash $checkruns ) "jobs running"
  sleep 10;
done ;

