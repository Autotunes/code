#!/bin/bash
export AUTOTUNEPATH=/lunarc/nobackup/users/jbellm/Tune/QTilde/FullAuto2
export AUTOTUNE_NFIND=1000
export AUTOTUNE_NSTEPS=4
export AUTOTUNE_ITERATIONS=2
checkruns=$AUTOTUNEPATH/mod/checkruns.sh
