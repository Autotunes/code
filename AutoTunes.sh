#!/bin/bash

# This script is the main script to call the subtunes and perform the tuning.
# The methods are described in 1908.10811. 

# The parameters are received by reading the env.sh and then given the
# folder structure searchsteps or subtunes are performed.

# If one of the steps fails the script should start at the last ext point.
# A bit debugging is needed by the user.


ITER="iterations"
set -e

source env.sh


# Determine how many iterations already have been made:
doneiterations=1
for j in $(seq $doneiterations $AUTOTUNE_ITERATIONS); do
  if [ -d "$ITER/$j" ]; then
    echo "iteration" $j "done"
    doneiterations=$(($j+1))
  fi
done

# Perform the open iterations:
for j in $(seq $doneiterations $AUTOTUNE_ITERATIONS); do
  # Check for SearchStep folder:
  if [ -d SearchStep ]; then
    echo "Folder SearchStep exists, skipping..."
  else
    # SearchStep has not yet been performed.. Call the scripts.
    python lib/prepareSearch.py 
    ( cd  SearchStep  ; bash $AUTOTUNEPATH/lib/runSearchStep.sh )
  fi
  # Get the slopes:
  if [ -f SearchStep/save.p ]; then
    echo "Slopes found in SearchStep..."
  else
    bash $AUTOTUNEPATH/mod/clusterRun.sh $AUTOTUNEPATH/mod/getSlopes.sh
    while [ "$( bash $checkruns )" -gt "0" ];
    do
        echo "getSlopes.py running..."
        sleep 10;
    done 
  fi
  # Perform the best guess tune:
  if ! [ -d $ITER ]; then
    mv param.start.dat param.start.dat.bak
    (cd SearchStep && if ! [ -f ipol.dat ]; then prof2-ipol scan ipol.dat --order=2 --limits=param.dat -q; fi && if ! [ -f bestGuessTune/results.txt ]; then prof2-tune -d `rivet-config --datadir` ipol.dat -r scan -o bestGuessTune -q --filter; fi  && python $AUTOTUNEPATH/lib/writeBestGuess.py)
  fi
  
  # Split the steps according to the weights described in 1908.10811.
  # Here we also write weight files:
  python $AUTOTUNEPATH/lib/splitSteps.py
  
  # Perform the sub-tunes.
  for i in $(seq 0 $(($AUTOTUNE_NSTEPS-1)))
    do
      ( cd TuneStep$i; bash $AUTOTUNEPATH/lib/runTuneStep.sh ; cat tune.step.res.dat >> $AUTOTUNEPATH/tune.res.dat )
    done
  # Move tune to iterations:
  if [ -d $ITER ]; then
    # Control will enter here if $DIRECTORY exists.
    echo "Folder $ITER exists"
  else
    echo "Creating folder $ITER"
    mkdir $ITER
  fi
  for i in $(seq 1 100); do
    if [ -d $ITER/$i ]; then
      echo "Folder $ITER/$i already exists..."
    else
      echo "Using folder $ITER/$i"
      mkdir $ITER/$i
      break
    fi
  done
  echo "Moving all relevant data to $ITER/${i}."
  mv SearchStep $ITER/$i
  mv TuneStep* $ITER/$i
  mv param.start.dat $ITER/$i
  if [ -f param.start.dat.bak ];then
    mv param.start.dat.bak $ITER/$i
  fi
  cp tune.res.dat $ITER/$i
  # Prepare next iteration by using the current tune as starting condition.
  mv tune.res.dat param.start.dat
  echo "Done with iteration $i."
done
echo "Plotting iterations."
python lib/iterPlot.py
